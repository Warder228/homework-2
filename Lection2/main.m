//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import <stdio.h>
#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"
#import "SPIPlayerSpaceship.h"

int main(int argc, const char * argv[]) {

    SPIStarSystem *alphaStarSystem = [[SPIStarSystem alloc] initWithName:@"Sirius" age:@(230000000)];
    
    SPIStar *alphaStar = [[SPIStar alloc] initWithName:@"Alpha"];
    alphaStar.mass = 123496464248;
    
    SPIPlanet *vulkanPlanet = [[SPIPlanet alloc] initWithName:@"Vulcan"];
    vulkanPlanet.atmosphere = YES;
    vulkanPlanet.peoplesCount = 528102000;

    SPIAsteroidField *hotaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Hota"];
    hotaAsteroidField.density = 6000000;
    
    SPITeleport *AlphaTeleporter = [[SPITeleport alloc] initWithName:@"Alpha Teleporter"];
    
    SPIPlanet *gallifreiPlanet = [[SPIPlanet alloc] initWithName:@"Gallifrey"];
    gallifreiPlanet.atmosphere = YES;
    gallifreiPlanet.peoplesCount = 700000000;
    
    SPIPlanet *nabooPlanet = [[SPIPlanet alloc] initWithName:@"Naboo"];
    nabooPlanet.atmosphere = YES;
    nabooPlanet.peoplesCount = 625000000;
    
    SPIPlanet *plutoPlanet = [[SPIPlanet alloc] initWithName:@"Pluto"];
    plutoPlanet.atmosphere = NO;
    
    SPIStarSystem *AndroSystem = [[SPIStarSystem alloc] initWithName:@"Andro" age:@(7223220000)];
    
    SPIStar *sunStar = [[SPIStar alloc] initWithName:@"Anreo"];
    sunStar.mass = 738552330000;
    
    SPITeleport *AnreoTeleporter = [[SPITeleport alloc] initWithName:@"Anreo Teleport"];
    
    SPIPlanet *VelesPlanet = [[SPIPlanet alloc] initWithName:@"Veles"];
    VelesPlanet.atmosphere = NO;
    VelesPlanet.peoplesCount = 0;
    
    SPIPlanet *TempoPlanet = [[SPIPlanet alloc] initWithName:@"Tempo"];
    TempoPlanet.atmosphere = YES;
    TempoPlanet.peoplesCount = 7000000000;
    
    SPIPlanet *SiezedPlanet = [[SPIPlanet alloc] initWithName:@"Siezed"];
    SiezedPlanet.atmosphere = NO;
    SiezedPlanet.peoplesCount = 0;
    
    SPIAsteroidField *cereraAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Sanet"];
    cereraAsteroidField.density = 50383222;
    
    AndroSystem.spaceObjects = [NSMutableArray arrayWithCapacity:6];
    [AndroSystem.spaceObjects addObject:AnreoTeleporter];
    [AndroSystem.spaceObjects addObject:sunStar];
    [AndroSystem.spaceObjects addObject:VelesPlanet];
    [AndroSystem.spaceObjects addObject:TempoPlanet];
    [AndroSystem.spaceObjects addObject:cereraAsteroidField];
    [AndroSystem.spaceObjects addObject:SiezedPlanet];
    
    alphaStarSystem.spaceObjects = [NSMutableArray arrayWithCapacity:7];
    [alphaStarSystem.spaceObjects addObject:AlphaTeleporter];
    [alphaStarSystem.spaceObjects addObject:vulkanPlanet];
    [alphaStarSystem.spaceObjects addObject:hotaAsteroidField];
    [alphaStarSystem.spaceObjects addObject:gallifreiPlanet];
    [alphaStarSystem.spaceObjects addObject:nabooPlanet];
    [alphaStarSystem.spaceObjects addObject:plutoPlanet];
    [alphaStarSystem.spaceObjects addObject:alphaStar];
    
    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"Semen"];
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:alphaStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:AndroSystem.spaceObjects];
    [gameObjects addObject:spaceship];
    
    NSMutableArray *StarSystemsArray = [[NSMutableArray alloc] init];
    [StarSystemsArray addObject:alphaStarSystem];
    [StarSystemsArray addObject:AndroSystem];
    spaceship.allStarSystems = StarSystemsArray;
    spaceship.currentStarSystem = StarSystemsArray[0];
    spaceship.currentSpaceObject = AlphaTeleporter;
    
    NSMutableArray *Teleports = [[NSMutableArray alloc] init];
    [Teleports addObject:AlphaTeleporter];
    [Teleports addObject:AnreoTeleporter];
    AlphaTeleporter.TeleportsArray = Teleports;
    AnreoTeleporter.TeleportsArray = Teleports;
    
    
    BOOL play = YES;
    
    while (play) {
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        
        for (id<SPIGameObject> gameObject in gameObjects) {
            [gameObject nextTurn];
        }
    }
    return 0;
}


