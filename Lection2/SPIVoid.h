//
//  SPIVoid.h
//  Lection2
//
//  Created by Admin on 06.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

@interface SPIVoid : SPISpaceObject

- (instancetype)initWithName:(NSString *)name;

@end
