//
//  SPIEPC.h
//  Lection2
//
//  Created by Admin on 05.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

@interface SPITeleport : SPISpaceObject

@property (nonatomic, strong) NSMutableArray *TeleportsArray;

- (instancetype)initWithName:(NSString *)name;

@end
