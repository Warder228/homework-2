//
//  SPIEPC.m
//  Lection2
//
//  Created by Admin on 05.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPITeleport.h"

@implementation SPITeleport

- (instancetype) initWithName:(NSString *)name
{
    self = [super initWithType:SPISpaceObjectTypeTeleport name:name];
    
    return self;
}

- (NSString *) description
{
    NSString *descr = nil;
    NSMutableArray *Destination = [[NSMutableArray alloc] init];
    long counter = 0;
    [Destination addObject:[NSString stringWithFormat: @"\nTeleport: %@", self.name]];
    for(SPITeleport *i in self.TeleportsArray)
    {
        if (i == self)
        {
            descr = [NSString stringWithFormat:@"%ld Stay here", counter];
        }
        else
        {
            descr = [NSString stringWithFormat:@"%ld Teleport to %@", counter, i.name];
        }
        [Destination addObject:descr];
        ++counter;
    }
    return [Destination componentsJoinedByString:@"\n"];
}

@end
