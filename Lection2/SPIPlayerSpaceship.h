//
//  PlayerSpaceship.h
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import "SPISpaceObject.h"
#import "SPIStarSystem.h"
#import "SPIGameObject.h"
#import "SPITeleport.h"
#import "SPIStar.h"
#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIVoid.h"

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipCommand) {
    SPIPlayerSpaceshipCommandFlyToPreviousObject = 1,
    SPIPlayerSpaceshipCommandExploreCurrentObject,
    SPIPlayerSpaceshipCommandFlyToNextObject,
    SPIPlayerSpaceshipCommandExit = 42,
};

typedef NS_ENUM(NSInteger, SPIPlayerSpaceshipResponse) {
    SPIPlayerSpaceshipResponseOK = 0,
    SPIPlayerSpaceshipResponseExit,
};

@interface SPIPlayerSpaceship : NSObject <SPIGameObject>

@property (nonatomic, strong, readonly) NSString *name;

@property (nonatomic, assign) int nextCommand;
@property (nonatomic, weak) SPIStarSystem *currentStarSystem;
@property (nonatomic, weak) SPISpaceObject *currentSpaceObject;
@property (nonatomic, weak) NSMutableArray *allStarSystems;

- (instancetype)initWithName:(NSString *)name;

- (NSString *)availableCommands;

- (SPIPlayerSpaceshipResponse)waitForCommand;

@end
