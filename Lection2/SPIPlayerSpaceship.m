//
//  PlayerSpaceship.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 17/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIPlayerSpaceship.h"

@implementation SPIPlayerSpaceship

- (instancetype)initWithName:(NSString *)name {
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

- (SPIPlayerSpaceshipResponse)waitForCommand {
    char command[255];

    printf("\n%s", [[self availableCommands] cStringUsingEncoding:NSUTF8StringEncoding]);
    printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fgets(command, 255, stdin);
    if(command[0] != '\n')
    {
        int commandNumber = atoi(command);
        self.nextCommand = commandNumber;
    }
    return self.nextCommand == 42 ? SPIPlayerSpaceshipResponseExit : SPIPlayerSpaceshipResponseOK;
}

- (void)nextTurn {
    
    if (self.nextCommand == [self.currentStarSystem.spaceObjects indexOfObject:self.currentSpaceObject])
    {
        if(self.currentSpaceObject.type == SPISpaceObjectTypeTeleport)
        {
            char flyTo[255];
            while(true)
            {
                printf("%s\n",[[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
                printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
                fgets(flyTo, 255, stdin);
                int FlyTo = atoi(flyTo);
                
                SPITeleport *buf = (SPITeleport *)self.currentSpaceObject;
                if((FlyTo >= 0) && (FlyTo < [buf.TeleportsArray count]))
                {
                    self.currentStarSystem = self.allStarSystems[FlyTo];
                    self.currentSpaceObject = _currentStarSystem.spaceObjects.firstObject;
                    break;
                }
            }
        }
        else
        {
            printf("%s\n",[[self.currentSpaceObject description] cStringUsingEncoding:NSUTF8StringEncoding]);
            
            char flyTo[255];
            while(true)
            {
                printf("\nDestroy it?\n0 - No\n1 - Yes");
                printf("\n%s ",[@"Input command:" cStringUsingEncoding:NSUTF8StringEncoding]);
                fgets(flyTo, 255, stdin);
                int NumberFire = atoi(flyTo);
                
                if((NumberFire == 1) || (NumberFire == 0))
                {
                    if (self.currentSpaceObject.type == SPISpaceObjectTypeStar)
                    {
                        SPIPlanet *newObj = [[SPIPlanet alloc] initWithName:self.currentSpaceObject.name];
                        newObj.atmosphere = NO;
                        newObj.peoplesCount = 0;
                        self.currentStarSystem.spaceObjects[[self.currentStarSystem.spaceObjects indexOfObject:self.currentSpaceObject]] = newObj;
                        self.currentSpaceObject = newObj;
                    }
                    else if (self.currentSpaceObject.type == SPISpaceObjectTypePlanet)
                    {
                        SPIAsteroidField *newObj = [[SPIAsteroidField alloc] initWithName:self.currentSpaceObject.name];
                        newObj.density = (unsigned int)arc4random() % 1000000;
                        self.currentStarSystem.spaceObjects[[self.currentStarSystem.spaceObjects indexOfObject:self.currentSpaceObject]] = newObj;
                        self.currentSpaceObject = newObj;
                    }
                    else if (self.currentSpaceObject.type == SPISpaceObjectTypeAsteroidField)
                    {
                        SPIVoid *newObj = [[SPIVoid alloc] initWithName:self.currentSpaceObject.name];
                        self.currentStarSystem.spaceObjects[[self.currentStarSystem.spaceObjects indexOfObject:self.currentSpaceObject]] = newObj;
                        self.currentSpaceObject = newObj;
                    }
                    else if (self.currentSpaceObject.type == SPISpaceObjectTypeVoid)
                    {
                        printf("\nSorry, we have no weapons for destroy the void\n");
                    }
                    break;
                }
            }
        }
    }
    else
    {
        if ((self.nextCommand >= 0) && (self.nextCommand < [self.currentStarSystem.spaceObjects count]))
        {
            self.currentSpaceObject = [self.currentStarSystem.spaceObjects objectAtIndex:self.nextCommand];
        }
    }
}

- (NSString *)availableCommands {
    if (self.currentStarSystem) {
        NSMutableArray *mutableDescriptions = [[NSMutableArray alloc] init];
        NSString *exploreDescription = nil;
        
        for (NSInteger command = 0; command < [self.currentStarSystem.spaceObjects count]; command++) {
            if (command == [self.currentStarSystem.spaceObjects indexOfObject:self.currentSpaceObject])
            {
                exploreDescription = [NSString stringWithFormat:@"%ld Explore %@",command, [self.currentStarSystem.spaceObjects[command] title]];
            }
            else
            {
                exploreDescription = [NSString stringWithFormat:@"%ld Fly to %@",command, [self.currentStarSystem.spaceObjects[command] title]];
            }
            [mutableDescriptions addObject:exploreDescription];
        }

        [mutableDescriptions addObject:[NSString stringWithFormat:@"%ld Quit", (long)SPIPlayerSpaceshipCommandExit]];
        
        return [mutableDescriptions componentsJoinedByString:@"\n"];
    }
    return nil;
}

@end
